;DNSdynamic credentials for the service(s) to update.

[Variables]
;Need to use the URL decode %40 for the '@' symbol
username=
password=

;hostname may be one host or a comma seperated list (with no spaces)
;for multiple hosts.  Leave hostname blank if you want to update all
;records at DNSdynamic with the same IP address.
hostname=